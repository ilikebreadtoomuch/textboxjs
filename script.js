var txtid = 0;
var eid = 0;
var maxtxtid = 4;
var snapsfx = new Audio('sounds/snap.ogg');

function changetxt(txtidf,eidf,txtf,bgf,txcf) {
	if (txtid == txtidf && eid == eidf) {
		document.getElementById("txt").innerHTML = txtf;
        if (bgf !== undefined){
            document.getElementById('bg').style.background = 'url("' + bgf + '")';
	    };
        if (txcf !== undefined){
            document.getElementById('txt').style.color = txcf;
        }
    };
};

function updatetxt(){
	changetxt(1,0,"Consectetur adipiscing elit");
    changetxt(2,0,"Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
    changetxt(3,0,"Ut enim ad minim veniam.");
    changetxt(4,0,"Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.","backgrounds/bg2.png","cyan");
};

document.onkeypress = function (e) {
    if(e.keyCode = 32  && txtid < maxtxtid) {
        snapsfx.play();
    	txtid += 1;
    	updatetxt();
    }
};